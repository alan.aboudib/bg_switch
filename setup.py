from setuptools import setup, find_packages

setup(
    name='bg_switch',
    version='0.1',
    description='Switch backgrounds between two images.',
    author='Alan ABOUDIB',
    author_email='agabudeeb@gmail.com',    
    packages=find_packages(),
    install_requires=[
        'latent-diffusion @ git+https://gitlab.com/alan.aboudib/latent-diffusion.git',
        'isnet @ git+https://gitlab.com/alan.aboudib/dis.git'
    ],
    entry_points={
        'console_scripts': [
            'bg_switch=bgswitch.main:main',
        ],
    },    

)
