## Intro

This is a python package you can use to switch backgrounds between two images.

## Main dependencies

The two main dependencies as you can see in the `setup.py` scripts are:



[alan.aboudib/DIS](https://gitlab.com/alan.aboudib/dis.git)

which is the foreground/background segmentation library I adapted from:

[xuebinqin/DIS](https://github.com/xuebinqin/DIS)


and:

[alan.aboudib/latent-diffusion](https://gitlab.com/alan.aboudib/latent-diffusion.git)


which is the latend diffusion library I adapted from:

[CompVis/latent-diffusion](https://github.com/CompVis/latent-diffusion.git)



## Installations

In order to install the library, you can simply use `pip`:

```
$ pip install git+https://gitlab.com/alan.aboudib/bg_switch.git
```


## Performing background switching


In order to switch backgrounds between two images, you can simply run:

```
$ bg_switch --image1_path <path to image1> --image2_path <path to image2> --steps 250
```

The resulting images with switched backgrounds will be stored in a folder called `switch_results` located in the working directory.

I found that 250 steps of sampling give the best results.

Running multiple times might give different results.




