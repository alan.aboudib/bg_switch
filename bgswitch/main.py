import argparse
import os
from PIL import Image

from isnet.segment_foreground import decompose_image
from ldm.inpaint import inpaint_image

from .utils import dilate_mask
from .utils import create_overlay


def main():

    parser = argparse.ArgumentParser()

    parser.add_argument("--image1_path", type = str, required = True,
                        help = "Path to the first image.")

    parser.add_argument("--image2_path", type = str, required = True,
                        help = "Path to the second image.")

    parser.add_argument("--steps", type = int, required = True,
                        help = "The number of sampling steps.")

    args = parser.parse_args()


    print("-- Foreground segmentation started ...")
    fg_image1, bg_image1, mask1 = decompose_image(args.image1_path)
    fg_image2, bg_image2, mask2 = decompose_image(args.image2_path)
    print("-- Finished.")

    # Keep track of original sizes
    img1_size = fg_image1.size
    img2_size = fg_image2.size

    print("-- Dilating masks ...")
    mask1_dilated = dilate_mask(mask1, kernel_size = 10, iterations = 15)
    mask2_dilated = dilate_mask(mask2, kernel_size = 10, iterations = 15)
    print("-- Finished.")

    # Due to memory limitations, images will be resized to 512x512

    bg_image1 = bg_image1.resize((512,512))
    bg_image2 = bg_image2.resize((512,512))

    mask1_dilated = mask1_dilated.resize((512,512))
    mask2_dilated = mask2_dilated.resize((512,512))


    # Now save the images to be inpainted along with their masks
    # in a temporary folder
    inpaint_inputs_path = './inpaint_inputs'
    inpaint_results_path = './inpaint_results'
    switch_results_path = './switch_results'

    if not os.path.exists(inpaint_inputs_path):
        os.mkdir(inpaint_inputs_path)

    if not os.path.exists(inpaint_results_path):
        os.mkdir(inpaint_results_path)

    if not os.path.exists(switch_results_path):
        os.mkdir(switch_results_path)

    bg_image1.save(os.path.join(inpaint_inputs_path,'bg_image1.png'))
    bg_image2.save(os.path.join(inpaint_inputs_path,'bg_image2.png'))

    mask1_dilated.save(os.path.join(inpaint_inputs_path,'bg_image1_mask.png'))
    mask2_dilated.save(os.path.join(inpaint_inputs_path,'bg_image2_mask.png'))


    # Start inpainting
    print(f"-- Inpainting images using {args.steps} sampling steps ...")
    inpaint_image(inpaint_inputs_path,
                  inpaint_results_path,
                  steps = args.steps)
    print("-- Finished.")

    print(f"** Inpainted images are saved to {inpaint_results_path}")


    # Load the inpainting results
    bg_image1_inpainted = Image.open(os.path.join(inpaint_results_path, "bg_image1.png")).resize(img1_size)
    bg_image2_inpainted = Image.open(os.path.join(inpaint_results_path, "bg_image2.png")).resize(img2_size)

    # Create the overlays (Background switch)
    print('-- Switching backgrounds ...')
    merged_image1 = create_overlay(fg_image = fg_image1,
                                   bg_image = bg_image2_inpainted,
                                   fg_mask = mask1)

    merged_image2 = create_overlay(fg_image = fg_image2,
                                   bg_image = bg_image1_inpainted,
                                   fg_mask = mask2)

    print('-- Finished.')

    # Saving the results
    merged_image1.save(os.path.join(switch_results_path, 'bg1_switched.png'))
    merged_image2.save(os.path.join(switch_results_path, 'bg2_switched.png'))    
    print(f"** Switch results are saved to {switch_results_path}")
