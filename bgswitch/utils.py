import numpy as np
import cv2
from PIL import Image

def get_start_end(dim1, dim2):
    """Let me explain with an example.
    If dim1 is bigger than dim2, then return
    the start and end coordinates of dim1 that
    fit dim2 in a centered fashion, and vice verca.

    |[start,end[ | = dim2
    """

    diff = abs(dim1 - dim2)

    if diff == 0:
        return 0, dim1

    start = int(np.ceil(diff / 2)) + 1
    end = max(dim1, dim2) - diff // 2 + 1

    return start, end

def get_img_indices(dim1, dim2):
    """Get the image indices necessary to overlay the images
    """
    
    if dim1 > dim2:
      rows1 = range(*get_start_end(dim1,dim2))
      rows2 = range(0,dim2)
    else:
      rows1 = range(0,dim1)
      rows2 = range(*get_start_end(dim1,dim2))

    return rows1, rows2

def dilate_mask(mask: Image, kernel_size = 5, iterations = 1):
    """Dilate the mask.
    This guarantees a better inpaint quality
    """

    mask = np.array(mask)
    
    kernel = np.ones((kernel_size, kernel_size), np.uint8)

    mask_dilated = cv2.dilate(mask, kernel, iterations = iterations)
    #mask_dilated = Image.fromarray(mask).filter(ImageFilter.MaxFilter(71))

    return Image.fromarray(mask_dilated)    

def create_overlay(fg_image: Image,
                   bg_image: Image,
                   fg_mask: Image) -> Image:
    """Superpose a foreground image on a background
    and return the result as a PIL image
    """

    # get the width/heigh of each image
    w1, h1 = fg_image.size
    w2, h2 = bg_image.size

    # Get the indices of the zone that
    # is common between both images
    cols1, cols2 = get_img_indices(w1,w2)
    rows1, rows2 = get_img_indices(h1,h2)

    # Create an inverted background binary mask
    fg_mask_inv = 1 - np.array(fg_mask) / 255
    fg_mask_inv_rgb = np.stack([fg_mask_inv]*3, axis=-1)

    # Superpose the foreground on the background
    fg_image_arr = np.array(fg_image)
    fg_image_arr[np.ix_(rows1, cols1)] += (fg_mask_inv_rgb[np.ix_(rows1, cols1)] * \
                                           np.array(bg_image)[np.ix_(rows2, cols2)]\
                                           ).astype(np.uint8)

    merged_image = Image.fromarray(fg_image_arr)

    return merged_image
